import './App.css';
import AddEvent from './components/Event-management/AddEvent';
import GetAllEvents from './components/Event-management/GetAllEvents';
import Registration from './components/Event-management/Registration';


import { Route, Switch } from 'react-router-dom/cjs/react-router-dom.min';
import DeleteEvent from './components/Event-management/DeleteEvent';
import UpdateEvent from './components/Event-management/UpdateEvent';
import Header from './components/Event-management/Header';
import EventManage from './components/Event-management/EventManage';
import Register from './components/Event-management/Register';
import SearchById from './components/Event-management/SearchById'
import SearchByLocation from './components/Event-management/SearchByLocation'
import Talks from './components/Event-management/Talks'



function App() {
  return (
    <div className="App">
    <Header/>
    <Switch>
      <Route exact path="/" component={EventManage} ></Route>
      <Route  path="/addevent" component={AddEvent} ></Route>
      <Route  path="/getAllEvents" component={GetAllEvents}></Route>
      <Route path ='/registerations' component ={Registration} ></Route>
      <Route path ='/talks' component ={Talks} ></Route>
      
      <Route path ='/deleteEvent' component ={DeleteEvent} ></Route>
      <Route path ='/updateEvent' component ={UpdateEvent}></Route>
      <Route path ='/search/location' component ={SearchByLocation}></Route>
      <Route path ='/search/id' component ={SearchById}></Route>
      <Route path ="/register/:value" component ={Register}></Route>
      
      


      </Switch> 
    
    </div>
  );
}

export default App;