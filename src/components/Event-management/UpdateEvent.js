import React, { Component } from 'react'
import axios from 'axios';


class UpdateEvent extends Component {
    constructor(){
        super();

        this.state ={
          eventId: 0,
          eventName : '',
          eventDesc : '',
          eventStatus : '',
          eventStart : '',
          eventEnd : '',
          eventLocation : ''
        }
        this.handleChange =this.handleChange.bind(this);
        this.clickHandler =this.clickHandler.bind(this);

    }

    clickHandler(e){
      
        let event ={
            eventName : this.state.eventName,
            description : this.state.eventDesc,
            status : this.state.eventStatus,
            startDate : this.state.eventStart,
            endDate : this.state.eventEnd,
            location : this.state.eventLocation
        }
        console.log("This is event : ",event)
        axios.put(`http://localhost:8080/v1/event/${this.state.eventId}`, event)
        .then(response =>alert(response.data))
        .catch(error => {
            this.setState({ errorMessage: error.message });
            console.error('There was an error!', error);
        });
        // axios({
        //   method: 'PUT',
        //   url: `http://localhost:8080/v1/event/${this.state.eventId}`,
        //   event
        // }).then(res=>{
        //   alert(res.data)
        //     })
    }

    handleChange(e){
        const name =e.target.name
        switch(name){
            case "id":
                this.setState({
                    eventId : e.target.value
                },()=>{
                  console.log(this.state.eventId)
                })
               
                break;
            case "Name":
                this.setState({
                    eventName : e.target.value
                })
                break;
            case "Desc":
                this.setState({
                    eventDesc : e.target.value
                })
                break;
            case "Status":
                this.setState({
                    eventStatus : e.target.value
                })
                console.log("kuch hua")
                break;
            case "startDate":
                this.setState({
                    eventStart : e.target.value
                })
                break;
            case "endDate":
                this.setState({
                    eventEnd : e.target.value
                })
                break;
            case "Location":
                this.setState({
                    location : e.target.value
                })
                break;
        }
    }

    render() {
        return (
            <div>
                 <label>
              Event Id: 
            </label>
            <input name ="id" type="number" onChange={(e) => {this.handleChange(e)}}/>
            <br/>
            <label>
              Event Name: 
            </label>
            <input name ="Name" type="text"  onChange={(e) => {this.handleChange(e)}}/>
            <br/>

            <label>
              Event Description: 
            </label>
            <input name ="Desc" type="text" onChange={(e) => {this.handleChange(e)}}/>
            <br/>

            <label>
              Event Status:
            </label>
            <select name ="Status" onChange={(e) => {this.handleChange(e)}}>
              <option value="Created">
                Created
              </option>
              <option value="In Progress">
                In Progress
              </option>
              <option value="Completed">
                Completed
              </option>
            </select>
           <br/>
            <label>
              Event Start Date: 
            </label>
            <input name="startDate" type="text" onChange={(e) => {this.handleChange(e)}}/>
            <br/>

            <label>
              Event End Date: 
            </label>
            <input name="endDate" type="text" onChange={(e) => {this.handleChange(e)}}/>
            <br/>
            
            <label>
              Event Location:
            </label>
            <select name="Location" onChange={(e) => {this.handleChange(e)}}>
              <option value="Bangalore">
                Bangalore
              </option>
              <option value="Moorsville">
                Moorsville
              </option>
              <option value="Online">
                Online
              </option>
            </select>

            <hr/>
            <button onClick={(e)=>{this.clickHandler(e)}}>
              Update Record
            </button>
          </div>
        )
    }
}

export default UpdateEvent