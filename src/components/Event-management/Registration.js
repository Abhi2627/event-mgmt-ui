import React, { Component } from 'react'
import axios from 'axios';

class Registration extends Component {
    constructor(){
        super();
        this.state ={
            response_data : []
        }
    }
    
    componentDidMount(){
        console.log("hello")
        axios.get("http://localhost:8080/v1/person/getall")
        .then( (response) => {
            const data =response.data;
            this.setState({
                response_data : data
            })
            console.log(data);
        })
        .catch(error => console.error("Error :", {error}));
    }
    render() {
        return (
            
            <div>
                <table border ='2'>
                    <tr>
                    <th>Person Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Organization</th>
                    <th>Designation</th>
                    <th>Email</th>
                    </tr>
                {this.state.response_data.map(
                    (person) =>(
                         <tr>
                              <td>{person.personId}</td>
                              <td>{person.fname} </td>
                              <td> {person.lname}</td>
                              <td> {person.organization}</td>
                              <td> {person.designation}</td>
                              <td> {person.email} </td>                            
                              </tr>
                    )
                )
                }
                </table>
            </div>
        )
    }
}

export default Registration