import axios from 'axios';
import React, { Component } from 'react'



class AddEvent extends Component {

    constructor(){
        super();
        this.state ={
          eventid: 0,
          eventName : '',
          eventDesc : '',
          eventStatus : '',
          eventStart : '',
          eventEnd : '',
          eventLocation : ''
        }
        this.handleChange =this.handleChange.bind(this);
        this.clickHandler =this.clickHandler.bind(this);

    }

    clickHandler(){
        const event ={
            eventName : this.state.eventName,
            description : this.state.eventDesc,
            status : this.state.eventStatus,
            startDate : this.state.eventStart,
            endDate : this.state.eventEnd,
            location : this.state.eventLocation
        }
        axios.post('http://localhost:8080/v1/event/',event)
        .then(res =>{
          console.log(res.data);
          alert(res.data)
        })

    }

    handleChange(e){
        const name =e.target.name
        console.log("inside function")
        switch(name){
           
            case "Name":
                this.setState({
                    eventName : e.target.value
                })
                console.log("kuch hua!!")
                break;
            case "Desc":
                this.setState({
                    eventDesc : e.target.value
                })
                console.log("kuch hua!!")
                break;
            case "Status":
                this.setState({
                    eventStatus : e.target.value
                })
                console.log("kuch hua!!")
                break;
            case "startDate":
                this.setState({
                    eventStart : e.target.value
                })
                console.log("kuch hua!!")
                break;
            case "endDate":
                this.setState({
                    eventEnd : e.target.value
                })
                break;
            case "Location":
                this.setState({
                    location : e.target.value
                })
                break;
        }
    }

    render() {
      const { eventid ,eventName ,eventDesc ,eventStatus ,eventStart, eventEnd ,eventLocation } =this.state;
        return (
            <div>

            <label>
              Event Name: 
            </label>
            <input name ="Name" type="text"  onChange={(e) => {this.handleChange(e)}} defaultValue ={eventName}/>
            <br/>

            <label>
              Event Description: 
            </label>
            <input name="Desc" type="text" onChange={(e) => {this.handleChange(e)}}/>
            <br/>

            <label>
              Event Status:
            </label>
            <select name="Status" onChange={(e) => {this.handleChange(e)}}>
              <option key ="option1" value="Created">
                Created
              </option>
              <option value="In Progress">
                In Progress
              </option>
              <option value="Completed">
                Completed
              </option>
            </select>
            <br/>
           
            <label>
              Event Start Date: 
            </label>
            <input name="startDate" type="text"  onChange={(e) => {this.handleChange(e)}}/>
            <br/>

            <label>
              Event End Date: 
            </label>
            <input name="endDate" type="text"  onChange={(e) => {this.handleChange(e)}}/>
            <br/>
            
            <label>
              Event Location:
            </label>
            <select name="Location"  onChange={(e) => {this.handleChange(e)}}>
              <option defaultValue="Bangalore">
                Bangalore
              </option>
              <option value="Moorsville">
                Moorsville
              </option>
              <option value="Online">
                Online
              </option>
            </select>

            <hr/>
            <button onClick={this.clickHandler}>
              Save Record
            </button>
          </div>
        )
    }
}

export default AddEvent