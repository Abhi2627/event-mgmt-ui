import React, { Component } from 'react'
import axios from 'axios';

class Talks extends Component {
    constructor(){
        super();
        this.state ={
            response_data : []
        }
    }
    
    componentDidMount(){
        console.log("hello")
        axios.get("http://localhost:8080/v1/talks/getall")
        .then( (response) => {
            const data =response.data;
            this.setState({
                response_data : data
            })
            console.log(data);
        })
        .catch(error => console.error("Error :", {error}));
    }
    render() {
        return (
            
            <div>
                <table border ='2'>
                    <tr>
                    <th>Topic Id</th>
                    <th>Topic Name</th>
                    <th>Description</th>
                    <th>Track</th>
                    <th>Speaker Name</th>
                    <th>Duration</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    
                    </tr>
                {this.state.response_data.map(
                    (talk) =>(
                         <tr>
                              <td>{talk.topicId}</td>
                              <td>{talk.topicName} </td>
                              <td> {talk.description}</td>
                              <td> {talk.track}</td>
                              <td> {talk.speakerName}</td>
                              <td> {talk.duration} </td>                            
                              <td> {talk.sStart} </td>                            
                              <td> {talk.sEnd} </td>                            
                              
                              </tr>
                    )
                )
                }
                </table>
            </div>
        )
    }
}

export default Talks