import axios from 'axios';
import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export default class SearchById extends Component {
    constructor(){
        super();
        this.state ={
            id: 0,
            event : {}
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleChange(e){
        this.setState({
            id: e.target.value
        })
        console.log("kuch hua!")
    }

    handleClick(id){
        // axios.get('http://localhost:8080/v1/event/',id)
        // .then(res =>{
        //     this.setState({
        //         event : res.data
        //     })
        // })

        axios({
            method: 'GET',
            url: `http://localhost:8080/v1/event/${id}`,
          }).then(res=>{
              this.setState({
                  event : res.data
              })
          },console.log(this.state.event))
    }

    render(){
        const {eventId,eventName,description,status,startDate,endDate,location}=this.state.event;
        return(
            <div>
                <input placeholder ="Enter Event Id : " type ='number' onChange={(e) => {this.handleChange(e)}} ></input> <br/>
                <button  onClick={()=>{this.handleClick(this.state.id)}} >Submit</button>
                <br/><br/>
                <table border ='2'>
                    <tr>
                    <th>Event Id</th>
                    <th>Event Name</th>
                    <th>Event Description</th>
                    <th>Event Status</th>
                    <th>Event Start Date</th>
                    <th>Event End Date</th>
                    <th>Event Location</th>
                   
                    
                    </tr>
                    <tr key ='event.eventId'>
                              <td>{eventId}</td>
                              <td> {eventName} </td>
                              <td> {description}</td>
                              <td> {status}</td>
                              <td> {startDate}</td>
                              <td> {endDate} </td>
                              <td> {location}</td>
                              </tr>
                        </table>
            </div>
        )
    }

}

