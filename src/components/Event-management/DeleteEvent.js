import axios from 'axios'
import React, { Component } from 'react'

class DeleteEvent extends Component {

    constructor(props) {
        super(props)
        this.state = {
             id : 0
        }
        this.deleteHandler =this.deleteHandler.bind(this);
        this.clickHandler =this.clickHandler.bind(this);
        
    }
 deleteHandler(e)  {
     this.setState({
            id : e.target.value
     })
     console.log("kuch hua!")
 }
 clickHandler(id){
     if(id >0){
        axios({
            method: 'DELETE',
            url: `http://localhost:8080/v1/event/${id}`,
          })
          .then(res => alert(res.data))
          .catch(error =>{
              console.log(error)
          }) 
          
     }
     else alert("Please enter Event Id")
 }
    render() {
        return (
            <div>
                <h2>Delete the Event</h2> <br/>
                <input onChange={(e) => {this.deleteHandler(e)}} placeholder="Input Event Id" ></input>
                <button onClick ={()=>{this.clickHandler(this.state.id)}} >Delete</button>
                
            </div>
        )
    }
}
export default DeleteEvent