import axios from 'axios';
import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export default class SeachByLocation extends Component {
    constructor(){
        super();
        this.state ={
            location: '',
            event : []
        }
        this.handleChange =this.handleChange.bind(this)
        this.handleClick =this.handleClick.bind(this)
    }
    handleChange(e){
        this.setState({
            location: e.target.value
        },()=>{
            console.log(this.state.location)
        })
    }

    handleClick(location){
        axios.get(`http://localhost:8080/v1/event/location=${location}`)
        .then(res =>{
            this.setState({
                event : res.data
            },()=>{
                console.log(this.state.event)
            })
        });
    }

    render(){

        return(
            <div>
                
                <input placeholder ="Enter Event Location : " type ='text' onChange ={(e)=>{this.handleChange(e)}} ></input> <br/>
                <button  onClick={()=>{this.handleClick(this.state.location)}} >Submit</button>
                <br/><br/>
                <table border ='2'>
                    <tr>
                    <th>Event Id</th>
                    <th>Event Name</th>
                    <th>Event Description</th>
                    <th>Event Status</th>
                    <th>Event Start Date</th>
                    <th>Event End Date</th>
                    <th>Event Location</th>
                    </tr>


                {this.state.event.map((event)=>(
                    <tr key ='event.eventId'>
                    <td>{event.eventId}</td>
                    <td>{event.eventName}</td>
                        <td>{event.description}</td>
                        <td>{event.status}</td>
                        <td>{event.startDate}</td>
                        <td>{event.endDate}</td>
                           <td>{event.location}</td>
                            </tr>
                            
  
                ))}
                </table>
                
            </div>
        )
    }

}



/*

export class GetEventsByLocation extends Component {
    constructor(props){
        super(props);
        this.state ={
            location : '',
            event : ''
        }
        this.locationref = React.createRef();
     
        this.locationHandler = this.locationHandler.bind(this);
        // this.submitHandler = this.submitHandler.bind(this);
    }

   locationHandler(e){
       if(e.target.name === "location")
        this.setState({ 
            location : e.target.value
        })
        if(e.target.name === "event")
        this.setState({
            location : e.target.value
        })
    }
    submitHandler(e){

       
        axios.get("/v1/event/location=",{
            params : {
                location : this.locationref.current.value
            }
        })
        .then((response) =>{
                this.setState({
                        event : JSON.stringify(response.data)
                })
        }
        
        );
    }
    render() {
        return (
            <div >
                <input ref = {this.locationref} type ='email' value ={this.state.location}> Enter Location to be searched : </input> <br/>
                <button  onClick ={this.submitHandler} >Submit {this.state.location}</button> <br/>
                <h2> Event Details : </h2>
                <p>{this.state.event}</p>

            </div>
        )
    }
}
*/