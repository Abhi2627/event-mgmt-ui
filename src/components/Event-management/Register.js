import React, { Component } from 'react'
import axios from 'axios';

class Register extends Component {
    constructor(props) {
        super(props)
        this.state ={
            id : parseInt(this.props.match.params.value),
            fName : '',
            lName : '',
            org : '',
            desig : '',
            email : ''
        }
        
        this.handleChange =this.handleChange.bind(this)
        this.clickHandler = this.clickHandler.bind(this)
    }

    handleChange(e){
        const name =e.target.name
        switch(name){
            
            case "Fname":
                this.setState({
                    fName : e.target.value
                })
                break;
            case "Lname":
                this.setState({
                    lName : e.target.value
                })
                break;
            case "Org":
                this.setState({
                    org : e.target.value
                })
                
                break;
            case "Desig":
                this.setState({
                    desig : e.target.value
                })
                break;
            case "Email":
                this.setState({
                    email : e.target.value
                })
                break;
        }
    }

    clickHandler(e){
        const people ={
            fname : this.state.fName,
            lname : this.state.lName,
            organization : this.state.org,
            designation : this.state.desig,
            email : this.state.email
        }
        console.log(people)

        axios.put(`http://localhost:8080/v1/event/${this.state.id}/register/`, people)
        .then(response => console.log(response.data))
        .catch(error => {
            console.error('There was an error!', error);
        });
    }
    

    render() {
        
        console.log(this.state.id);
        return (
            <div>
                   <label>
              Event Id : {this.state.id}
            </label>
            <br/>
            <label>
              First Name : 
            </label>
            <input name ="Fname" type="text"  onChange={(e) => {this.handleChange(e)}}/>
            <br/>
            <label>
              Last Name : 
            </label>
            <input name ="Lname" type="text"  onChange={(e) => {this.handleChange(e)}}/>
            <br/>
            <label>
              Organization : 
            </label>
            <input name ="Org" type="text" onChange={(e) => {this.handleChange(e)}}/>
            <br/>
            <label>
            Designation : 
            </label>
            <input name ="Desig" type="text" onChange={(e) => {this.handleChange(e)}}/>
            <br/>

            <label>
            E-mail : 
            </label>
            <input name ="Email" type="text" onChange={(e) => {this.handleChange(e)}}/>
            <br/>
            
             <button onClick={(e)=>{this.clickHandler(e)}}>
             Register
           </button>
           </div>
        )
    }
}
export default Register
