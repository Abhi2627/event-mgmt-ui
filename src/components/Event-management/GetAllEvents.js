import React, { Component } from 'react'
import axios from 'axios';
import {Link} from 'react-router-dom'

class GetAllEvents extends Component {
    constructor(){
        super();
        this.state ={
            response_data : []
        }
    }
    componentDidMount(){
        axios.get("http://localhost:8080/v1/event/getall")
        .then( (response) => {
            const data =response.data;
            this.setState({
                response_data : data
            })
        })
        .catch(error => console.error("Error :", {error}));
    }
    render() {
        const btnstyle ={
            backgroundColor: "pink",
                                border: "none",
                                color: "white",
                                padding: "15px",
                                textAlign: "center",
                                textDecoration: "none",
                                display: "inline-block",
                                fontSize: "16px"
        }
        return (
            
            <div>
                <table border ='2'>
                    <tr>
                    <th>Event Id</th>
                    <th>Event Name</th>
                    <th>Event Description</th>
                    <th>Event Status</th>
                    <th>Event Start Date</th>
                    <th>Event End Date</th>
                    <th>Event Location</th>
                    <th>Register Link</th>
                    
                    </tr>
                {this.state.response_data.map(
                    (event) =>(
                         <tr key ='event.eventId'>
                              <td>{event.eventId}</td>
                              <td> {event.eventName} </td>
                              <td> {event.description}</td>
                              <td> {event.status}</td>
                              <td> {event.startDate}</td>
                              <td> {event.endDate} </td>
                              <td> {event.location}</td>
                              <Link style={btnstyle}  to={`/register/${event.eventId}`}>Register</Link>
                              {console.log(event.eventId)}
                              </tr>
                    )
                )
                }
                </table>
            </div>
        )
    }
}

export default GetAllEvents